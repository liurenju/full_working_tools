import java.util.Scanner;
import java.io.File;

public class TimeParser{
	public static void main(String []args) throws Exception{
		Scanner scanner_args = new Scanner(System.in);
		//System.out.println("Please enter the name of events: ");
		String events = scanner_args.next();
		//System.out.println("Please enter the file path of .html: ");
		String path = scanner_args.next();

		File file = new File(path);
		Scanner scanner_file = new Scanner(file);
		boolean search = false;
		boolean start = true;
		while(scanner_file.hasNextLine()){
			String line = scanner_file.nextLine();
			String []parsed = line.split(" ");
			if(line.startsWith("#")) search = true;
			if(search){
				if(line.contains(events)){
					int i = 0;
					for(; i < parsed.length; i++){
						if(parsed[i].endsWith(":")){
							double temp = Double.parseDouble(parsed[i].substring(0, parsed[i].length()-1)+" ");
							temp = temp * 1000000;
							System.out.printf("%.0f ", temp);
							if(!start){
								System.out.println("");
								System.exit(0);
							}
							start = false;
							break;
						}
					}
				}
			}
		}
		System.err.println("NO SUCH EVENTS FOUND!!!!! RESTART THE PROGRAM");
	}
}